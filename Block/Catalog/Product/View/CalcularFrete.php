<?php

namespace Uptogo\Magento\Block\Catalog\Product\View;

use \Magento\Catalog\Block\Product\View\AbstractView;
use \Magento\Catalog\Block\Product\Context;
use \Magento\Framework\Stdlib\ArrayUtils;
use \Uptogo\Magento\Helper\Data;

class CalcularFrete extends AbstractView {
  protected $_helper;

  public function __construct(
    Context $context,
    ArrayUtils $arrayUtils,
    Data $helper,
    array $data = []
  ) {
    $this->_helper = $helper;
    parent::__construct($context, $arrayUtils, $data);
  }

  public function canShow(){
    return $this->getProduct()->isSalable() && $this->_helper->getActive();
  }
}
