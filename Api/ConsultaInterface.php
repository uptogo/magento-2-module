<?php

namespace Uptogo\Magento\Api;

interface ConsultaInterface {
  /**
   * Consulta Cep
   *
   * @param string $cep
   * @return \Uptogo\Magento\Api\Data\Dados|Bool
   * @throws \Magento\Framework\Exception\NoSuchEntityException
   */
  public function consultaCep($cep);

  /**
   * Estima frete do produto
   * @return string
   */
  public function estimarFrete();
}
