#!/bin/bash
./commit.sh "v$1"
git tag -f "v$1"
git push --delete origin "v$1"
git push origin "v$1"
