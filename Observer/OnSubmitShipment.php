<?php
namespace Uptogo\Magento\Observer;

use \Magento\Framework\Event\Observer;
use \Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\UrlInterface;
use \Magento\Framework\App\ResponseFactory;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\Message\ManagerInterface;
use \Magento\Sales\Model\Order\Address;
use \Uptogo\Magento\Helper\Data;
use \GoogleMapsHelper\GoogleMapsHelper;
use \Uptogo\Sdk\Uptogo;

class OnSubmitShipment implements ObserverInterface {
  protected $_scopeConfig;
  protected $_messageManager;
  protected $_responseFactory;
  protected $_url;
  protected $_helper;

  public function __construct(
    ScopeConfigInterface $scopeConfig,
    ManagerInterface $messageManager,
    ResponseFactory $responseFactory,
    UrlInterface $url,
    Data $helper
  ) {
    $this->_scopeConfig = $scopeConfig;
    $this->_messageManager = $messageManager;
    $this->_responseFactory = $responseFactory;
    $this->_url = $url;
    $this->_helper = $helper;
  }

  private function formatShippingAddress(Address $address) {
    $result = "";
    if (sizeof($address->getStreet()) > 0) {
      foreach ($address->getStreet() as $streetIndex => $street) {
        if ($streetIndex > 0) {
          $result .= ', ';
        }
        $result .= $street;
      }
    }
    if ($address->getPostcode() !== null) {
      $result .= ', ' . $address->getPostcode();
    }
    if ($address->getRegion() !== null) {
      $result .= ', ' . $address->getRegion();
    }
    if ($address->getCity() !== null) {
      $result .= ', ' . $address->getCity();
    }
    if ($address->getCountryId() !== null) {
      $result .= ', ' . $address->getCountryId();
    }
    return $result;
  }

  private function suspend(
    int $orderId,
    string $msg = 'Endereço do cliente não encontrado.'
  ) {
    $this->_messageManager->addError(
      $msg . '<br>' .
      'Crie um novo pedido manualmente clicando ' .
      '<a target="_blank" href="' .
      'https://uptogo.com.br/app/usuario/logar' .
      '">aqui</a>.'
    );
    $this->_responseFactory->create()->setRedirect(
      $this->_url->getUrl(
        "adminhtml/order_shipment/new",
        array('order_id' => $orderId)
      )
    )->sendResponse();
    exit;
  }

  public function execute(Observer $observer) {
    $order = $observer->getEvent()->getShipment()->getOrder();
    $shippingMethod = $order->getShippingMethod();
    if (strpos($shippingMethod, 'uptogo') !== false) {
      $shippingAddress = $order->getShippingAddress();
      try {
        $googleMapsHelper = new GoogleMapsHelper(
          $this->_helper->getGoogleMapsApiKey()
        );
        $origin = $googleMapsHelper->geocode(
          $this->_helper->getOriginStreetLine1() . ' - ' .
          $this->_helper->getOriginStreetLine2() . ' - ' .
          $this->_helper->getOriginZipCode() . ' - ' .
          $this->_helper->getOriginCity()
        );
        if (sizeof($origin) > 0) {
          $origin = $origin[0];
          $destination = $googleMapsHelper->geocode(
            $this->formatShippingAddress($shippingAddress)
          );
          if (sizeof($destination) > 0) {
            $destination = $destination[0];
            $route = $googleMapsHelper->directions($origin, $destination);
            if (sizeof($route) > 0) {
              $route = $route[0];
              $uptogo = new Uptogo($this->_helper->getApiKey());
              $trackPassword = 'mg2_order_' . $order->getId();
              if (strpos($shippingMethod, 'express') !== false) {
                $uptogoOrder = $uptogo->calcExpress($route);
                $uptogoOrder = $uptogo->newExpress($uptogoOrder, $trackPassword);
              } else {
                $uptogoOrder = $uptogo->calcEcommerce($route);
                $uptogoOrder = $uptogo->newEcommerce($uptogoOrder, $trackPassword);
              }
              if ($uptogoOrder->getId() === null) {
                $this->suspend(
                  $order->getId(),
                  "Não foi possível criar um novo pedido."
                );
              } else {
                $comment = $order->addStatusHistoryComment(
                  'O código de rastreamento da entrega é ' .
                  '<a target="_blank" href="' .
                  'https://uptogo.com.br/app/pedido/rastrear/' .
                  $uptogoOrder->getId() .
                  '">#' . $uptogoOrder->getId() . '</a>' .
                  ' e a senha de acesso é ' . $trackPassword . '.'
                );
                $comment->setIsCustomerNotified(1)->save();
                $order->save();
              }
            } else {
              $this->suspend(
                $order->getId(),
                "Não foi possível criar uma rota."
              );
            }
          } else {
            $this->suspend(
              $order->getId(),
              "Verifique as configurações de endereço da loja."
            );
          }
        } else {
          $this->suspend($order->getId());
        }
      } catch (\Exception $e) {
        $this->suspend(
          $order->getId(),
          "Verifique as chaves da API no módulo UPtogo."
        );
      }
    }
    return $observer;
  }
}
