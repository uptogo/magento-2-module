<?php

namespace Uptogo\Magento\Model\Endereco;

class Viacep {

	public static function getEndereco($cep) {
		$html = self::_request('http://viacep.com.br/ws/' . $cep . '/json/');
		$json = json_decode($html, 1);
		if ($json) {
			$dados = array(
				'logradouro' => $json['logradouro'],
				'bairro' => $json['bairro'],
				'cep' => (int)$cep,
				'cidade' => $json['localidade'],
				'uf' => strtoupper($json['uf'])
			);
			if (strpos($dados['logradouro'], ' - ') !== false) {
				$l = explode(' - ', $dados['logradouro']);
				$dados['logradouro'] = $l[0];
			}
			return $dados;
		}
		return false;
	}

	public static function _request($url, $get = array()) {
		$ch = curl_init($url);
		curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		return curl_exec($ch);
	}
}
