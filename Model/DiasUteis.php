<?php

namespace Uptogo\Magento\Model;

class DiasUteis implements \Magento\Framework\Option\ArrayInterface {
    
    /**
     * @return array
     */
    public function toOptionArray() {
        return [            
            ['value' => 1, 'label' => __('Segunda-feira')],
            ['value' => 2, 'label' => __('Terça-feira')],
            ['value' => 3, 'label' => __('Quarta-feira')],
            ['value' => 4, 'label' => __('Quinta-feira')],
            ['value' => 5, 'label' => __('Sexta-feira')],
            ['value' => 6, 'label' => __('Sábado')],
            ['value' => 7, 'label' => __('Domingo')]
        ];
    }
}
