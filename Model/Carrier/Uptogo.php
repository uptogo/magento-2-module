<?php

namespace Uptogo\Magento\Model\Carrier;

use \Magento\Quote\Model\Quote\Address\RateRequest;
use \Magento\Shipping\Model\Rate\Result;
use \Magento\Shipping\Model\Carrier\AbstractCarrier;
use \Magento\Shipping\Model\Carrier\CarrierInterface;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use \Psr\Log\LoggerInterface;
use \Magento\Shipping\Model\Rate\ResultFactory;
use \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use \Uptogo\Magento\Helper\Data;
use \Uptogo\Sdk\Uptogo as UptogoSdk;
use \GoogleMapsHelper\GoogleMapsHelper;

class Uptogo extends AbstractCarrier implements CarrierInterface {
  /**
   * @var string
   */
  protected $_code = 'uptogo';

  /**
   * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
   * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
   * @param \Psr\Log\LoggerInterface $logger
   * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
   * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
   * @param \Uptogo\Magento\Helper\Data $helper
   * @param array $data
   */
  public function __construct(
    ScopeConfigInterface $scopeConfig,
    ErrorFactory $rateErrorFactory,
    LoggerInterface $logger,
    ResultFactory $rateResultFactory,
    MethodFactory $rateMethodFactory,
    Data $helper,
    array $data = []
  ) {
    $this->_rateResultFactory = $rateResultFactory;
    $this->_rateMethodFactory = $rateMethodFactory;
    $this->_helper = $helper;
    parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
  }

  private function isDiaUtil() {
    return in_array(
      (int) $this->_helper->getNow()->format('N'),
      $this->_helper->getDiasUteis()
    );
  }

  private function isExpediente() {
    return (
      $this->_helper->getNow() >= $this->_helper->getInicioExpediente()
      &&
      $this->_helper->getNow() <= $this->_helper->getFimExpediente()
    );
  }

  private function durationToHours(int $duration) {
    return round($duration / (60 * 60), 0);
  }

  private function durationToDays(int $duration) {
    return round($this->durationToHours($duration) / 24, 0);
  }

  private function addUnitToDuration($duration, $unit) {
    $text = "{$duration} {$unit}";
    if ($duration > 1) $text .= 's';
    return $text;
  }

  private function formatAsHours(int $duration) {
    return $this->addUnitToDuration(
      $this->durationToHours($duration),
      'hora'
    );
  }

  private function formatAsDays(int $duration) {
    return $this->addUnitToDuration(
      $this->durationToDays($duration),
      'dia'
    );
  }

  private function replaceHours(int $duration, $text) {
    return str_replace(
      '%horas',
      $this->formatAsHours($duration),
      $text
    );
  }

  private function replaceDays(int $duration, $text) {
    return str_replace(
      '%dias',
      $this->formatAsDays($duration),
      $text
    );
  }

  private function replaceDuration(int $duration) {
    return $this->replaceDays(
      $duration,
      $this->replaceHours(
        $duration,
        $this->_helper->getFormatoPrazo()
      )
    );
  }

  private function durationToString(int $duration) {
    if ($this->_helper->getAbreviarData()) {
      $durationInDays = $this->durationToDays($duration);
      if ($durationInDays < 1) {
        return 'Chega hoje';
      } else if ($durationInDays < 2) {
        return 'Chega amanhã';
      }
    }
    return $this->replaceDuration($duration);
  }

  private function getNextDiaUtil() {
    $diaUtil = $this->_helper->getInicioExpediente();
    do {
      $diaUtil->modify('+1 day');
    } while (
      !in_array(
        (int) $diaUtil->format('N'),
        $this->_helper->getDiasUteis()
      )
    );
    return $diaUtil;
  }

  private function incrementDuration(int $duration) {
    if (!$this->isDiaUtil() || !$this->isExpediente()) {
      $duration += (
        $this->getNextDiaUtil()->getTimestamp()
        -
        $this->_helper->getNow()->getTimestamp()
      );
    }
    return $duration;
  }

  private function formatDuration(int $duration) {
    return $this->durationToString(
      $this->incrementDuration(
        $duration
      )
    );
  }

  /**
   * @return array
   */
  public function getAllowedMethods() {
    return ['uptogo' => $this->getConfigData('name')];
  }

  /**
   * @param RateRequest $request
   * @return bool|Result
   */
  public function collectRates(RateRequest $request) {
    if (!$this->getConfigFlag('active')) {
      return false;
    }
    try {
      $uptogo = new UptogoSdk($this->getConfigData('api_key'));
      if (!$uptogo->validateCep($request->getDestPostcode())) {
        return false;
      }
      $googleMapsHelper = new GoogleMapsHelper($this->getConfigData('google_maps_api_key'));
      $origin = $googleMapsHelper->geocode(
        $this->_helper->getOriginStreetLine1() . ' - ' .
        $this->_helper->getOriginStreetLine2() . ' - ' .
        $this->_helper->getOriginZipCode() . ' - ' .
        $this->_helper->getOriginCity()
      );
      $destination = $googleMapsHelper->geocode(
        $this->_helper->complementAddress(
          $request->getDestPostcode()
        )
      );

      if (sizeof($origin) > 0 && sizeof($destination) > 0) {
        $route = $googleMapsHelper->directions($origin[0], $destination[0]);
        if (sizeof($route) > 0) {
          $result = $this->_rateResultFactory->create();
          $cupom = strlen($this->getConfigData('cupom')) === 6 ? $this->getConfigData('cupom') : null;

          $expressOrder = $uptogo->calcExpress(clone $route[0], $cupom);
          $express = $this->_rateMethodFactory->create();
          $express->setCarrier('uptogo');
          $express->setCarrierTitle('UPtogo');
          $express->setMethod('uptogo_express');
          $express->setMethodTitle(
            'UPtogo - Expresso - ' . $this->formatDuration(
              $expressOrder->getRoute()->getDuration()
            )
          );
          $express->setPrice($expressOrder->getPrice());
          $express->setCost($expressOrder->getPrice());

          $ecommerceOrder = $uptogo->calcEcommerce(clone $route[0], $cupom);
          $ecommerce = $this->_rateMethodFactory->create();
          $ecommerce->setCarrier('uptogo');
          $ecommerce->setCarrierTitle('UPtogo');
          $ecommerce->setMethod('uptogo_ecommerce');
          $ecommerce->setMethodTitle(
            'UPtogo - E-Commerce - ' . $this->formatDuration(
              $ecommerceOrder->getRoute()->getDuration()
            )
          );
          $ecommerce->setPrice($ecommerceOrder->getPrice());
          $ecommerce->setCost($ecommerceOrder->getPrice());

          $result->append($express);
          $result->append($ecommerce);

          return $result;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } catch (Exception $e) {
      return false;
    }
  }
}
