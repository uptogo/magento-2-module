<?php

namespace Uptogo\Magento\Model;

use \Magento\Framework\DataObject;
use \Magento\Directory\Model\Region;
use \Magento\Quote\Model\Quote;
use \Magento\Framework\App\Request\Http;
use \Magento\Catalog\Model\Product;
use \Magento\Quote\Model\Quote\TotalsCollector;
use \Magento\Framework\Pricing\Helper\Data as PricingHelper;
use \GoogleMapsHelper\GoogleMapsHelper;
use \Uptogo\Magento\Api\Data\Dados;
use \Uptogo\Magento\Api\ConsultaInterface;
use \Uptogo\Magento\Model\Endereco\Correios;
use \Uptogo\Magento\Model\Endereco\Republicavirtual;
use \Uptogo\Magento\Model\Endereco\Viacep;
use \Uptogo\Magento\Helper\Data as UptogoHelper;

class Consulta extends DataObject implements Dados, ConsultaInterface {
  protected $_quote;
  protected $_modelRegion;
  protected $_request;
  protected $_product;
  protected $_totalsCollector;
  protected $_priceHelper;
  protected $_helperSigep;

  public function __construct(
    Region $modelRegion,
    Quote $quote,
    Http $request,
    Product $product,
    TotalsCollector $totalsCollector,
    PricingHelper $priceHelper,
    UptogoHelper $dataHelper
  ) {
    $this->_quote = $quote;
    $this->_modelRegion = $modelRegion;
    $this->_request = $request;
    $this->_product = $product;
    $this->_totalsCollector = $totalsCollector;
    $this->_priceHelper = $priceHelper;
    $this->_dataHelper = $dataHelper;
  }

  public function consultaCep($cep){
    try {
      $address = Correios::getEndereco($cep);
      if (!$address) {
        $address = Viacep::getEndereco($cep);
        if (!$address) {
          $address = Republicavirtual::getEndereco($cep);
        }
      }

      if ($address) {
        $cep = $address['logradouro'] .
        $address['bairro'] .
        $address['cep'] .
        $address['cidade'] .
        $address['uf'];
      }

      $googleMapsHelper = new GoogleMapsHelper($this->_dataHelper->getGoogleMapsApiKey());
      $addresses = $googleMapsHelper->geocode($cep);
      if (
        sizeof($addresses) > 0
        &&
        $addresses[0]->getDistrict() !== null
      ) {
        if ($addresses[0]->getZipCode() === null) {
          $addresses[0]->setZipCode($cep);
        }
        $this->setData('logradouro', $addresses[0]->getStreet());
        $this->setData('cep', $addresses[0]->getZipCode());
        $this->setData('bairro', $addresses[0]->getDistrict());
        $this->setData('cidade', $addresses[0]->getCity());
        $this->setData('uf', $addresses[0]->getState() !== null ?
          $this->getRegionId($addresses[0]->getState()) :
          null
        );
        return $this;
      } else {
        return false;
      }
    } catch (\Exception $e) {
      return false;
    }
  }

  public function estimarFrete() {
    $request = $this->_request->getParams();
    $params = new DataObject();
    $params->setData($request);
    $this->_product->load($request['product']);
    $this->_quote->addProduct($this->_product, $params);
    $this->_quote->collectTotals();
    $shipping = $this->_quote->getShippingAddress();
    $shipping->setCountryId('BR');
    $shipping->setPostcode($request['cep']);
    $shipping->setCollectShippingRates(true);
    $this->_totalsCollector->collectAddressTotals($this->_quote, $shipping);
    $rates = $shipping->collectShippingRates()->getAllShippingRates();
    $data = array();
    if (count($rates)) {
      foreach ($rates as $k => $rate) {
        $data[$k]['title'] = $rate->getMethodTitle();
        $data[$k]['price'] = $this->_priceHelper->currency($rate->getPrice(), true, false);
      }
      return json_encode($data);
    } else {
      throw new \Exception('Não foi possivel calcular frete.');
    }
  }

  public function getRegionId($uf, $country_id = "BR") {
    return $this->_modelRegion->loadByCode($uf, $country_id)->getId();
  }

  public function getLogradouro() {
    return $this->getData('logradouro');
  }

  public function getBairro() {
    return $this->getData('bairro');
  }

  public function getCep() {
    return $this->getData('cep');
  }

  public function getCidade() {
    return $this->getData('cidade');
  }

  public function getUf() {
    return $this->getData('uf');
  }

  public function getError() {
    return $this->getData('error');
  }
}
