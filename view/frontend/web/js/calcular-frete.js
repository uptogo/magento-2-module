define([
  'jquery',
  'mask',
  'Magento_Checkout/js/checkout-data'
], ($, mask, storage) => {
  'use strict';

  let dom = {};
  dom.form = $('form.uptogo.calcular-frete');
  dom.cep = $(dom.form).find('.fields > input[type="text"]');
  dom.submit = $(dom.form).find('.fields > input[type="submit"]');
  dom.results = $(dom.form).find('.results');

  $(dom.cep).mask('00000-000');

  if (storage.getShippingAddressFromData()) {
    $(dom.cep).val(storage.getShippingAddressFromData().postcode);
  }

  $(dom.form).on('submit', (e) => {
    $(dom.results).slideUp();
    e.preventDefault();

    let send = { cep: $(dom.cep).val() };
    $.each($('#product_addtocart_form').serializeArray(), (i, field) => {
      if (field.name.search('super_attribute') !== -1) {
        if (field.value == "") {
          $(dom.results).html('');
          $(dom.results).append(
            '<li>Configure o produto antes de calcular o frete.</li>'
          );
          $(dom.results).slideDown('slow');
          return false;
        }
      }
      send[field.name] = field.value;
    });

    $.ajax({
      url: $(dom.form).attr('action'),
      method: 'get',
      data: send,
      beforeSend: () => {
        $(dom.submit).val('Aguarde...');
      },
      success: (data) => {
        $(dom.results).html('');
        $.each($.parseJSON(data), (i, carrier) => {
          $(dom.results).append(`<li>${carrier.title} - ${carrier.price}</li>`);
        });
        $(dom.results).slideDown('slow');
        $(dom.submit).val('Calcular frete');
        if (storage.getShippingAddressFromData()) {
          var storageData = storage.getShippingAddressFromData();
          storageData.postcode = $(dom.cep).val();
          storage.setShippingAddressFromData(storageData);
        } else {
          storage.setShippingAddressFromData({
            postcode: $(dom.cep).val(),
            country_id: '',
            region: '',
            region_id: ''
          });
        }
      },
      error: () => {
        $(dom.submit).val('Calcular frete');
        $(dom.results).html('Erro ao calcular frete, tente novamente.');
        $(dom.results).slideDown('slow');
      }
    });
  });
});
