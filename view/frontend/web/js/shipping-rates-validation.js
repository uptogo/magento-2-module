/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/shipping-rates-validator',
        'Magento_Checkout/js/model/shipping-rates-validation-rules',
        'Uptogo_Magento/js/model/shipping-rates-validator',
        'Uptogo_Magento/js/model/shipping-rates-validation-rules'
    ],
    function (
        Component,
        defaultShippingRatesValidator,
        defaultShippingRatesValidationRules,
        uptogoShippingRatesValidator,
        uptogoShippingRatesValidationRules
    ) {
        'use strict';
        defaultShippingRatesValidator.registerValidator('uptogo', uptogoShippingRatesValidator);
        defaultShippingRatesValidationRules.registerRules('uptogo', uptogoShippingRatesValidationRules);
        return Component;
    }
);
