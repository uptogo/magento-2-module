<?php

namespace Uptogo\Magento\Helper;

use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Uptogo\Magento\Model\Endereco\Correios;
use \Uptogo\Magento\Model\Endereco\Republicavirtual;
use \Uptogo\Magento\Model\Endereco\Viacep;

class Data {
  protected $_scopeConfig;

  public function __construct(ScopeConfigInterface $scopeConfig) {
    $this->_scopeConfig = $scopeConfig;
  }

  public function getActive() {
    return $this->_scopeConfig->getValue('carriers/uptogo/active');
  }

  public function getApiKey() {
    return $this->_scopeConfig->getValue('carriers/uptogo/api_key');
  }

  public function getCupom() {
    return $this->_scopeConfig->getValue('carriers/uptogo/cupom');
  }

  public function getFormatoPrazo() {
    return $this->_scopeConfig->getValue('carriers/uptogo/formato_prazo');
  }

  public function getAbreviarData() {
    return $this->_scopeConfig->getValue('carriers/uptogo/abreviar_data');
  }

  public function getDiasUteis() {
    return array_map(
      'intval',
      explode(
        ',',
        $this->_scopeConfig->getValue('carriers/uptogo/dias_uteis')
      )
    );
  }

  private function timeToDateTime($configValue) {
    $date = $this->getNow();
    call_user_func_array(
      Array(
        $date,
        'setTime'
      ),
      array_map(
        'intval',
        explode(
          ',',
          $configValue
        )
      )
    );
    return $date;
  }

  public function getInicioExpediente() {
    return $this->timeToDateTime(
      $this->_scopeConfig->getValue('carriers/uptogo/inicio_expediente')
    );
  }

  public function getFimExpediente() {
    return $this->timeToDateTime(
      $this->_scopeConfig->getValue('carriers/uptogo/fim_expediente')
    );
  }

  public function getNow() {
    return new \DateTime("now", new \DateTimeZone('America/Sao_Paulo'));
  }

  public function getOriginStreetLine1() {
    return $this->_scopeConfig->getValue('shipping/origin/street_line1');
  }

  public function getOriginStreetLine2() {
    return $this->_scopeConfig->getValue('shipping/origin/street_line2');
  }

  public function getOriginZipCode() {
    return $this->_scopeConfig->getValue('shipping/origin/postcode');
  }

  public function getOriginCity() {
    return $this->_scopeConfig->getValue('shipping/origin/city');
  }

  public function getGoogleMapsApiKey() {
    return $this->_scopeConfig->getValue('carriers/uptogo/google_maps_api_key');
  }

  public function complementAddress(string $zipCode, string $number = '') {
    $result = $zipCode;
    $address = Correios::getEndereco($zipCode);
    if (!$address) {
      $address = Viacep::getEndereco($zipCode);
      if (!$address) {
        $address = Republicavirtual::getEndereco($zipCode);
      }
    }
    if ($address) {
      $result = $address['logradouro'] . ', ' .
      $number . ', ' .
      $address['bairro'] . ', ' .
      $address['cep'] . ', ' .
      $address['cidade'] . ', ' .
      $address['uf'];
    }
    return $result;
  }
}
